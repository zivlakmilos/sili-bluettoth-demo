package rs.zivlak.silibluetoothdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleWriteCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CommunicationActivity extends AppCompatActivity {

    private static final String uuidTimeService =             "0000c000-0000-1000-8000-00805f9b34fb";
    private static final String uuidTimeCharacteristicWrite = "0000c001-0000-1000-8000-00805f9b34fb";
    private static final String uuidLedService =              "0000a000-0000-1000-8000-00805f9b34fb";
    private static final String uuidLedCharacteristicWrite =  "0000a001-0000-1000-8000-00805f9b34fb";

    private Button m_btnSendTime;
    private Button m_btnLedOn;
    private Button m_btnLedOff;

    private BleDevice m_bleDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);

        m_btnSendTime = findViewById(R.id.btnSendTime);
        m_btnLedOn = findViewById(R.id.btnLedOn);
        m_btnLedOff = findViewById(R.id.btnLedOff);

        m_bleDevice = (BleDevice)getIntent().getExtras().get("BleDevice");

        setupHandlers();
    }

    private void setupHandlers() {
        m_btnSendTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //long time = System.currentTimeMillis() / 1000L;
                long time = calculateCurrentTime();
                byte[] arr = ByteBuffer.allocate(4).putInt((int)time).array();
                final byte[] data = reverseByteArray(arr);

                Log.d("bluetooth", "System time: " + String.valueOf(time));
                Log.d("bluetooth", "Time data: " + bytesToHexString(data));
                sendData(uuidTimeService, uuidTimeCharacteristicWrite, data);
            }
        });

        m_btnLedOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] data = { 0x01 };
                sendData(uuidLedService, uuidLedCharacteristicWrite, data);
            }
        });

        m_btnLedOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] data = { 0x00 };
                sendData(uuidLedService, uuidLedCharacteristicWrite, data);
            }
        });
    }

    private void sendData(String uuidService, String uuidCharacteristic, byte[] data) {
        BleManager.getInstance().write(
                m_bleDevice,
                uuidService,
                uuidCharacteristic,
                data,
                new BleWriteCallback() {
                    @Override
                    public void onWriteSuccess(int current, int total, byte[] justWrite) {
                        Toast.makeText(getApplicationContext(), "Senging success", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onWriteFailure(BleException exception) {
                        Toast.makeText(getApplicationContext(), exception.getDescription(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private String bytesToHexString(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for(byte b : bytes){
            sb.append(String.format("0x%02x", b & 0xff));
        }
        return sb.toString();
    }

    private byte[] reverseByteArray(byte[] arr) {
        byte[] res = new byte[arr.length];
        for(int  i = 0; i < arr.length / 2; i++){
            res[i] = arr[arr.length -i -1];
            res[arr.length - i - 1] = arr[i];
        }
        return res;
    }

    private long calculateCurrentTime() {
        TimeZone tz = TimeZone.getDefault();
        Calendar calendar = Calendar.getInstance(tz);
        calendar.setTimeZone(tz);
        long offset = calendar.get(Calendar.ZONE_OFFSET) +
                      calendar.get(Calendar.DST_OFFSET);

        return (calendar.getTime().getTime() + offset) / 1000L;
    }
}
