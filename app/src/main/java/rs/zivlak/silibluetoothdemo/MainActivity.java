package rs.zivlak.silibluetoothdemo;

import android.bluetooth.BluetoothGatt;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleGattCallback;
import com.clj.fastble.callback.BleScanCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;
import com.clj.fastble.scan.BleScanRuleConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private SwipeRefreshLayout m_deviceListLayout;
    private ListView m_deviceList;

    private List<String> m_deviceListDisplay = new ArrayList<String>();
    private List<BleDevice> m_deviceListData;

    private ArrayAdapter<String> m_deviceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_deviceListLayout = findViewById(R.id.deviceListLayout);
        m_deviceList = findViewById(R.id.deviceList);

        m_deviceListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, m_deviceListDisplay);
        m_deviceList.setAdapter(m_deviceListAdapter);

        initializeBluetooth();
        setupHandlers();
        refreshDeviceList();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BleManager.getInstance().disconnectAllDevice();
        BleManager.getInstance().destroy();
    }

    private void initializeBluetooth() {
        BleManager.getInstance().init(getApplication());
        BleManager.getInstance()
                .enableLog(true)
                .setReConnectCount(1, 5000)
                .setConnectOverTime(20000)
                .setOperateTimeout(5000);

        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder()
                .setServiceUuids(null)
                .setDeviceName(true, null)
                .setDeviceMac(null)
                .setAutoConnect(false)
                .setScanTimeOut(10000)
                .build();
        BleManager.getInstance().initScanRule(scanRuleConfig);
    }

    private void setupHandlers() {
        m_deviceListLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshDeviceList();
            }
        });

        m_deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String device = m_deviceListDisplay.get(position);  // TODO: Remove this and uncomment line bellow
                BleDevice device = m_deviceListData.get(position);
                final int pos = position;

                BleManager.getInstance().connect(device, new BleGattCallback() {
                    @Override
                    public void onStartConnect() {
                    }

                    @Override
                    public void onConnectFail(BleDevice bleDevice, BleException exception) {
                        Toast.makeText(getApplicationContext(), exception.getDescription(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
                        Intent intent = new Intent(getBaseContext(), CommunicationActivity.class);
                        intent.putExtra("BleDevice", m_deviceListData.get(pos));
                        startActivity(intent);
                    }

                    @Override
                    public void onDisConnected(boolean isActiveDisConnected, BleDevice device, BluetoothGatt gatt, int status) {
                    }
                });

                /*
                 * TODO: Remove
                 */
                /*
                Intent intent = new Intent(getBaseContext(), CommunicationActivity.class);
                intent.putExtra("BleDevice", device);
                startActivity(intent);
                */
            }
        });
    }

    private void refreshDeviceList() {
        BleManager.getInstance().scan(new BleScanCallback() {
            @Override
            public void onScanFinished(List<BleDevice> scanResultList) {
                Log.d("bluetooth", "Scan result list size: " + String.valueOf(scanResultList.size()));

                m_deviceListLayout.setRefreshing(false);
                m_deviceListData = scanResultList;
                m_deviceListDisplay.clear();
                for (BleDevice ble : scanResultList) {
                    String deviceDisplay = ble.getName() + "\n" + ble.getMac();
                    m_deviceListDisplay.add(deviceDisplay);
                }
                m_deviceListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onScanStarted(boolean success) {
                m_deviceListLayout.setRefreshing(success);
                if (!success)
                    Toast.makeText(getApplicationContext(), "DEVICE DOES NOT SUPPORT BLE", Toast.LENGTH_SHORT).show();

                /*
                 * TODO: Remove
                 *       This is test because my test device does not support BLE
                 */
                /*
                m_deviceListDisplay.add("Test 1\nuuid-1");
                m_deviceListDisplay.add("Test 2\nuuid-2");
                m_deviceListDisplay.add("Test 3\nuuid-3");
                m_deviceListDisplay.add(UUID.randomUUID().toString());
                m_deviceListAdapter.notifyDataSetChanged();
                */
            }

            @Override
            public void onScanning(BleDevice bleDevice) {
            }
        });
    }
}
